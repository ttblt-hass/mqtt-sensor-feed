"""Mqtt Kab switch module."""
import asyncio
import os
from datetime import datetime, timezone
from typing import Any, Optional, Dict

import pytz
import paho.mqtt.client as mqtt
import yaml

from mqtt_hass_base.daemon import MqttClientDaemon
from mqtt_sensor_feed.facebook_device import FacebookFeedDevice
from mqtt_sensor_feed.imap_device import ImapFeedDevice
from mqtt_sensor_feed.const import MAIN_LOOP_WAIT_TIME


class MqttSensorFeed(MqttClientDaemon):
    """MQTT Sensor Feed."""

    def __init__(self):
        """Create a new MQTT Sensor Feed object."""
        MqttClientDaemon.__init__(self, "mqtt-sensor-feed")

    def read_config(self):
        """Read env vars."""
        self.config_file = os.environ["CONFIG_YAML"]
        with open(self.config_file, "rb") as fhc:
            self.config = yaml.safe_load(fhc)
        self.sync_frequency = int(
            self.config.get("sync_frequency", MAIN_LOOP_WAIT_TIME)
        )
        self._forced_timezone = pytz.timezone(
            self.config.get(
                "timezone", datetime.now(timezone.utc).astimezone().tzname()
            )
        )

        self.message_dates_cache = self.config.get(
            "cache_file", "/tmp/mqtt_sensor_feed.db"
        )
        if "CACHE_FILE" in os.environ:
            self.message_dates_cache = os.environ["CACHE_FILE"]

        if "facebook" in self.config:
            self._fb_device = FacebookFeedDevice(
                self.name + "-facebook",
                self.logger,
                "homeassistant",
                "mqtt-sensor-feed",
                self.config["facebook"],
                self.message_dates_cache,
                self._forced_timezone,
            )
            self._fb_device.add_entities()

        if "imap" in self.config:
            self._imap_device = ImapFeedDevice(
                self.name + "-imap",
                self.logger,
                "homeassistant",
                "mqtt-sensor-feed",
                self.config["imap"],
                self.message_dates_cache,
            )
            self._imap_device.add_entities()

    async def _init_main_loop(self):
        """Init before starting main loop."""
        if hasattr(self, "_imap_device"):
            await self._imap_device.imap_connect()

    async def _set_devices_mqtt_client(self):
        """Set mqtt_client to all devices."""
        if hasattr(self, "_fb_device"):
            self._fb_device.set_mqtt_client(self.mqtt_client)
        if hasattr(self, "_imap_device"):
            self._imap_device.set_mqtt_client(self.mqtt_client)

    async def _main_loop(self):
        """Run main loop."""
        if hasattr(self, "_fb_device"):
            self._fb_device.update()
        if hasattr(self, "_imap_device"):
            await self._imap_device.update()

        i = 0
        while i < MAIN_LOOP_WAIT_TIME and self.must_run:
            await asyncio.sleep(1)
            i += 1

    def _on_connect(self, client, userdata, flags, rc):
        """MQTT on connect callback."""
        if hasattr(self, "_fb_device"):
            self._fb_device.register()
        if hasattr(self, "_imap_device"):
            self._imap_device.register()

    def _on_publish(self, client, userdata, mid):
        """MQTT on publish callback."""

    def _mqtt_subscribe(self, client, userdata, flags, rc):
        """Subscribe to all needed MQTT topic."""
        if hasattr(self, "_fb_device"):
            self._fb_device.subscribe()
        if hasattr(self, "_imap_device"):
            self._imap_device.subscribe()

    def _signal_handler(self, signal_, frame):
        """Handle SIGKILL."""
        if hasattr(self, "_fb_device"):
            self._fb_device.unregister()
        if hasattr(self, "_imap_device"):
            self._imap_device.unregister()

    def _on_message(self, client, userdata, msg):
        """Do nothing."""

    def _on_disconnect(
        self,
        client: mqtt.Client,
        userdata: Optional[Dict[str, Any]],
        rc: int,  # pylint: disable=invalid-name
    ) -> None:
        """On disconnect callback."""

    async def _loop_stopped(self):
        """Run after the end of the main loop."""
        if hasattr(self, "_imap_device"):
            self._imap_device.stop()
