"""MQTT Sensor Facebook module."""
import logging
from datetime import datetime, timezone, timedelta
import re
import shelve
from typing import Dict
from urllib.parse import parse_qs, urlparse

from bs4 import BeautifulSoup
from fb_feed_gen import fetch
from mqtt_hass_base.device import MqttDevice
import requests

from mqtt_sensor_feed.__version__ import VERSION
from mqtt_sensor_feed.common import render_value, clean_string


class FacebookFeedDevice(MqttDevice):
    """MQTT Sensor Facebook class."""

    def __init__(
        self,
        name: str,
        logger: logging.Logger,
        mqtt_discovery_root_topic: str,
        mqtt_data_root_topic: str,
        config: Dict,
        message_dates_cache: str,
        forced_timezone,
    ):
        """Create a new MQTT Sensor Facebook object."""
        MqttDevice.__init__(
            self, name, logger, mqtt_discovery_root_topic, mqtt_data_root_topic
        )
        self._config = config
        self.message_dates_cache = message_dates_cache
        # Get that
        # self.mac = raw_device_info.mac_address
        self.sw_version = VERSION
        self.manufacturer = "ttblt-hass"
        self.model = "mqtt-sensor-feed-facebook"
        self._forced_timezone = forced_timezone
        self._local_timezone = datetime.now(timezone.utc).astimezone().tzinfo
        self.identifiers = self.name

    def add_entities(self):
        """Add Home Assistant entities."""
        for fb_conf in self._config.get("sensors", []):
            # TODO improve this cleanup
            cleaned_name = fb_conf["name"].lower().replace(" ", "_")
            entity_settings = {
                "device_class": fb_conf.get("device_class", None),
                "expire_after": fb_conf.get("expire_after", 0),
                "force_update": fb_conf.get("force_update", False),
            }
            if "icon" in fb_conf:
                entity_settings["icon"] = fb_conf["icon"]

            setattr(
                self,
                cleaned_name,
                self.add_entity(
                    entity_type="sensor",
                    name=f"facebook {cleaned_name}",
                    unique_id=f"facebook_{cleaned_name}",
                    entity_settings=entity_settings,
                ),
            )

    def update(self):
        """Update Home Assistant entities."""
        for fb_conf in self._config.get("sensors", []):
            cleaned_name = fb_conf["name"].lower().replace(" ", "_")

            site_url = fetch.build_site_url(fb_conf["page"])
            data = fetch.get_remote_data(site_url)
            self.logger.debug("Fetching post from %s", site_url)
            posts = fetch.extract_items(fb_conf["page"], data, self.logger)
            if not posts:
                self.logger.info("Extracted post list is empty")
                continue

            posts.reverse()
            # parse all posts not read yet
            for post in posts:
                last_msg_date = self._get_last_message_date(fb_conf["name"])
                if post["date"] <= last_msg_date:
                    continue
                # TODO find a way to read all the messages
                # use an other lib ?
                msg = short_msg = BeautifulSoup(post["article"], "lxml").text
                if BeautifulSoup(post["article"], "lxml"):
                    art = BeautifulSoup(post["article"], "lxml")
                    for link in art.findAll("a"):
                        if link.text == "More":
                            try:
                                query = parse_qs(urlparse(link.attrs["href"]).query)
                                story_id = query.get("story_fbid")
                                if story_id:
                                    res = requests.get(link.attrs["href"])
                                    post_page = BeautifulSoup(res.content, "lxml")
                                    msg = (
                                        post_page.find(
                                            "div", {"data-ft": re.compile(story_id[0])}
                                        )
                                        .find("div", {"data-ft": '{"tn":"*s"}'})
                                        .text
                                    )
                                    break
                            except Exception:  # pylint: disable=broad-except
                                continue
                # Filter messages and titles
                matched = False
                for filter_ in fb_conf["filters"]:
                    if re.match(filter_, post["title"]) or re.match(filter_, msg):
                        matched = True
                if matched is False:
                    continue

                # Handle timezone
                post_date = (
                    post["date"]
                    .replace(tzinfo=self._local_timezone)
                    .astimezone(self._forced_timezone)
                )

                template_data = {
                    "title": post["title"],
                    "date": post_date.strftime("%Y-%m-%d %H:%M"),
                    "cleaned_message": clean_string(msg),
                    "message": msg,
                    "short_message": short_msg,
                }
                self.logger.info("New message detected: %s", post["title"])
                # Build attributes
                attributes = {}
                for key, value in fb_conf.get("attributes", {}).items():
                    attributes[key] = render_value(value, template_data)
                # Build state
                if "state" in fb_conf:
                    state = render_value(fb_conf["state"], template_data)
                else:
                    state = " - ".join((template_data["date"], template_data["title"]))
                # Send state
                getattr(self, cleaned_name).send_state(state, attributes)
                getattr(self, cleaned_name).send_available()
                # Save last message date
                self._save_last_message_date(fb_conf["name"], post["date"])

    def _get_last_message_date(self, sensor_name):
        """Get last email ID."""
        with shelve.open(self.message_dates_cache) as ids_by_name:
            message_id = ids_by_name.get(
                sensor_name, datetime.now() - timedelta(days=2)
            )
        return message_id

    def _save_last_message_date(self, sensor_name, message_date):
        """Save last email ID."""
        with shelve.open(self.message_dates_cache) as ids_by_name:
            ids_by_name[sensor_name] = message_date
