FROM python:3.9-slim-buster
  
RUN mkdir -p /app
WORKDIR /app

COPY requirements.txt /app/requirements.txt

RUN apt-get update -y && \
    apt-get upgrade -y && \
    apt-get install git gcc -y && \
    pip install pip --upgrade && \
    pip install -r /app/requirements.txt && \
    apt-get purge git -y && \
    apt-get autoremove -y && \
    apt-get clean && \
    rm -rf /var/lib/apt


COPY Dockerfile /Dockerfile
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh

COPY setup.py test_requirements.txt /app/
COPY mqtt_sensor_feed /app/mqtt_sensor_feed

RUN python setup.py install

ENV MQTT_USERNAME=username
ENV MQTT_PASSWORD=password
ENV MQTT_HOST=192.168.0.1
ENV MQTT_PORT=1883
ENV ROOT_TOPIC=homeassistant
ENV MQTT_NAME=mqtt-sensor-feed
ENV LOG_LEVEL=INFO
ENV CACHE_FILE /tmp/mqtt_sensor_feed.db

CMD sh /entrypoint.sh
